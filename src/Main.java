import input.ConsoleInput;
import input.FileInput;
import input.GeneratedMatrixInput;
import input.Input;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) {
        Input input = createInputFromArgs(args);
        Application app = new Application(input);
        app.start();
    }

    public static Input createInputFromArgs(String[] args) {
        String filename = "";
        boolean generate = false;

        for (String arg : args) {
            if (arg.startsWith("-f=") || arg.startsWith("--file=")) {
                filename = args[0].split("=", 2)[1];
            }
            else if (arg.equals("-g") || arg.equals("--generate")) {
                generate = true;
            }
            else {
                System.out.println("Неизвестный параметр: " + args[0]);
                System.out.println("Доступные параметры:");
                System.out.println("--file или -f: задать имя файла для ввода");
                System.out.println("--generate или -g: автоматическая генерация уравнений");
                System.exit(1);
            }
        }

        return createInput(filename, generate);
    }

    public static Input createInput(String filename, boolean generate) {
        if (generate) {
            return new GeneratedMatrixInput(createInput(filename, false), -5, 20);
        }

        if (!filename.isEmpty()) {
            try {
                return new FileInput(new FileInputStream(filename));
            } catch (FileNotFoundException e) {
                System.out.println("Файл не найден: " + filename);
            }
        }
        return new ConsoleInput();
    }
}
