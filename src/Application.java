import equations.*;
import exceptions.InvalidEquationException;
import exceptions.DiagonalizationException;
import exceptions.RangeException;
import exceptions.SolvingException;
import input.Input;
import util.ArrayUtils;

import java.math.BigDecimal;
import java.util.NoSuchElementException;

public class Application {
    private final Input input;

    public Application(Input input) {
        this.input = input;
    }

    public void start() {
        try {
            int iterations = input.getIterations();
            BigDecimal epsilon = input.getEpsilon();
            int size = input.getMatrixSize();
            BigDecimal[][] matrix = input.getMatrix(size);

            try {
                Equations equations = new Equations(matrix);
                System.out.println("Изначальная матрица:");
                equations.print();
                System.out.println();

                makeDiagonalDominant(equations);
                NormalizedEquations normalized = new NormalizedEquations(equations);
                solveEquations(normalized, iterations, epsilon);
            } catch (InvalidEquationException e) {
                System.out.println("Ошибка! Неправильный ввод матрицы коэффициентов");
            }
        }
        catch (NoSuchElementException e) {
            System.out.println("Ошибка чтения ввода. Проверьте введенные данные и попробуйте снова");
        }
        catch (NumberFormatException e) {
            System.out.println("Ошибка чтения числа");
        }
        catch (RangeException e) {
            if (e.getMessage().isEmpty()) {
                System.out.println("Ошибка чтения данных. Число выходит за допустимый диапазон");
            }
            else {
                System.out.println("Ошибка чтения данных. " + e.getMessage());
            }
        }
    }

    private void makeDiagonalDominant(Equations equations) {
        try {
            DiagonalBalancer.makeDiagonalDominant(equations);
            System.out.println("Успешно достигнуто диагональное преобладание:");
            equations.print();
            System.out.println();
        }
        catch (DiagonalizationException e) {
            System.out.println("Внимание! Невозможно достигнуть диагонального преобладания");
        }
    }

    private void solveEquations(NormalizedEquations equations, int iterations, BigDecimal epsilon) {
        try {
            Solver solver = new Solver(iterations, epsilon);
            Solution result = solver.solve(equations);
            System.out.println("Решение найдено за " + result.iterations() + " итераций");
            System.out.println("Вектор решений:\n" + ArrayUtils.toString(result.solution()));
            System.out.println("Вектор погрешностей:\n" + ArrayUtils.toString(result.difference()));
        }
        catch (SolvingException e) {
            System.out.println("Решение системы, которое удовлетворяет введенным параметрам, не найдено");
        }
    }
}
