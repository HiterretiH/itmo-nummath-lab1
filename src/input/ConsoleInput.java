package input;

import java.math.BigDecimal;
import java.util.Scanner;

public class ConsoleInput implements Input {
    private final Scanner scanner;

    public ConsoleInput() {
        scanner = new Scanner(System.in);
    }

    public int getInt() {
        while (true) {
            try {
                return Integer.parseInt(scanner.next());
            }
            catch (NumberFormatException e) {
                scanner.nextLine();
                System.out.print(("Ошибка! Введите целое число: "));
            }
        }
    }

    public int getInt(String message, int min, int max) {
        System.out.print(message + ": ");
        int value = getInt();

        while (value < min || value > max) {
            System.out.println("Введите число в диапазоне от " + min + " до " + max);
            System.out.print(message + ": ");
            value = getInt();
        }

        return value;
    }

    public BigDecimal getDecimal() {
        while (true) {
            try {
                return new BigDecimal(scanner.next().replace(',', '.'));
            }
            catch (NumberFormatException e) {
                scanner.nextLine();
                System.out.print("Ошибка! Введите число: ");
            }
        }
    }

    public BigDecimal getDecimal(String message, BigDecimal min, BigDecimal max) {
        System.out.print(message + ": ");
        BigDecimal value = getDecimal();

        while (value.compareTo(min) < 0 || value.compareTo(max) > 0) {
            System.out.println("Введите число в диапазоне от " +
                    min.toString().replace("E", "*10^") +
                    " до " + max.toString().replace("E", "*10^"));
            System.out.print(message + ": ");
            value = getDecimal();
        }

        return value;
    }

    @Override
    public int getIterations() {
        return getInt("Введите количество итераций", 1, 100000000);
    }

    @Override
    public BigDecimal getEpsilon() {
        return getDecimal("Введите требуемую погрешность", new BigDecimal("0.0000001"),  BigDecimal.ONE);
    }

    @Override
    public int getMatrixSize() {
        return getInt("Введите размер системы", 1, 20);
    }

    @Override
    public BigDecimal[][] getMatrix(int matrixSize) {
        System.out.println("Введите коэффициенты уравнений:");

        BigDecimal[][] matrix = new BigDecimal[matrixSize][matrixSize + 1];
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize + 1; j++) {
                matrix[i][j] = getDecimal();
            }
        }

        return matrix;
    }
}
