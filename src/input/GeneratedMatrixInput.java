package input;

import equations.MatrixGenerator;
import exceptions.RangeException;

import java.math.BigDecimal;
import java.util.NoSuchElementException;

public class GeneratedMatrixInput implements Input {
    private final Input input;
    private final double min;
    private final double max;

    public GeneratedMatrixInput(Input input, double min, double max) {
        this.input = input;
        this.min = min;
        this.max = max;
    }

    @Override
    public int getIterations() throws NumberFormatException, NoSuchElementException, RangeException {
        return input.getIterations();
    }

    @Override
    public BigDecimal getEpsilon() throws NumberFormatException, NoSuchElementException, RangeException {
        return input.getEpsilon();
    }

    @Override
    public int getMatrixSize() throws NumberFormatException, NoSuchElementException, RangeException {
        return input.getMatrixSize();
    }

    @Override
    public BigDecimal[][] getMatrix(int matrixSize) throws NumberFormatException, NoSuchElementException, RangeException {
        return MatrixGenerator.generate(matrixSize, min, max);
    }
}
