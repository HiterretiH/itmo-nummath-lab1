package input;

import exceptions.RangeException;

import java.math.BigDecimal;
import java.util.NoSuchElementException;

public interface Input {
    int getIterations() throws NumberFormatException, NoSuchElementException, RangeException;
    BigDecimal getEpsilon() throws NumberFormatException, NoSuchElementException, RangeException;
    int getMatrixSize() throws NumberFormatException, NoSuchElementException, RangeException;
    BigDecimal[][] getMatrix(int matrixSize) throws NumberFormatException, NoSuchElementException, RangeException;
}
