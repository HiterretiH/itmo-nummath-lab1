package input;

import exceptions.RangeException;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class FileInput implements Input {
    private final Scanner scanner;

    public FileInput(FileInputStream in) {
        scanner = new Scanner(in);
    }

    private BigDecimal getDecimal() throws NumberFormatException, NoSuchElementException {
        return new BigDecimal(scanner.next().replace(',', '.'));
    }

    private int getInt() throws NumberFormatException, NoSuchElementException {
        return Integer.parseInt(scanner.next());
    }

    @Override
    public int getIterations() throws NumberFormatException, NoSuchElementException, RangeException {
        int value = getInt();
        if (value < 1 || value > 100000000) {
            throw new RangeException("Количество итераций должно быть от 1 до 100000000");
        }

        return value;
    }

    @Override
    public BigDecimal getEpsilon() throws NumberFormatException, NoSuchElementException, RangeException {
        BigDecimal value = getDecimal();

        if (value.compareTo(new BigDecimal("0.0000001")) < 0 || value.compareTo(BigDecimal.ONE) > 0) {
            throw new RangeException("Погрешность должна быть от 0.0000001 до 1");
        }

        return value;
    }

    @Override
    public int getMatrixSize() throws NumberFormatException, NoSuchElementException, RangeException {
        int value = getInt();
        if (value < 1 || value > 20) {
            throw new RangeException("Размер системы должен быть от 1 до 20");
        }

        return value;
    }

    @Override
    public BigDecimal[][] getMatrix(int matrixSize) throws NumberFormatException, NoSuchElementException {
        BigDecimal[][] matrix = new BigDecimal[matrixSize][matrixSize + 1];
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize + 1; j++) {
                matrix[i][j] = getDecimal();
            }
        }

        return matrix;
    }
}
