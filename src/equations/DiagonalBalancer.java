package equations;

import exceptions.DiagonalizationException;
import util.ArrayUtils;

import java.math.BigDecimal;

public class DiagonalBalancer {
    public static void makeDiagonalDominant(Equations equations) throws DiagonalizationException {
        BigDecimal[][] a = equations.getA();
        BigDecimal[] b = equations.getB();

        int[] maxAt = findMaxIndexes(a);

        for (int i = 0; i < a.length; i++) {
            if (i != maxAt[i]) {
                ArrayUtils.swapArrays(a[i], a[maxAt[i]]);
                ArrayUtils.swapElements(b, i, maxAt[i]);
                ArrayUtils.swapElements(maxAt, i, maxAt[i]);
            }
        }

        for (int i = 0; i < a.length; i++) {
            if (i != maxAt[i]) {
                throw new DiagonalizationException();
            }
        }
    }

    private static int[] findMaxIndexes(BigDecimal[][] a) {
        int[] maxAt = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            maxAt[i] = 0;
            for (int j = 0; j < a.length; j++) {
                if (a[i][j].abs().compareTo(a[i][maxAt[i]].abs()) > 0) {
                    maxAt[i] = j;
                }
            }
        }

        return maxAt;
    }
}
