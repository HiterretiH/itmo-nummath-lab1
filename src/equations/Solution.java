package equations;

import java.math.BigDecimal;

public record Solution(int iterations, BigDecimal[] solution, BigDecimal[] difference) {
}
