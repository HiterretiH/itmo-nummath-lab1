package equations;

import exceptions.SolvingException;

import java.math.BigDecimal;

public class Solver {
    private final int iterations;
    private final BigDecimal epsilon;

    public Solver(int iterations, BigDecimal epsilon) {
        this.iterations = iterations;
        this.epsilon = epsilon;
    }

    public Solution solve(NormalizedEquations equations) throws SolvingException {
        BigDecimal[][] c = equations.getC();
        BigDecimal[] d = equations.getD();
        int size = c.length;

        BigDecimal[] previous = new BigDecimal[size];
        BigDecimal[] difference = new BigDecimal[size];
        BigDecimal[] solution = new BigDecimal[size];

        System.arraycopy(d, 0, solution, 0, size);

        int iteration = 0;
        do {
            iteration++;
            BigDecimal maxDifference = BigDecimal.ZERO;
            System.arraycopy(solution, 0, previous, 0, size);

            for (int i = 0; i < size; i++) {
                solution[i] = d[i];
                for (int j = 0; j < size; j++) {
                    solution[i] = solution[i].add(c[i][j].multiply(previous[j]));
                }

                difference[i] = solution[i].subtract(previous[i]).abs();
                maxDifference = maxDifference.max(difference[i]);
            }

            if (maxDifference.compareTo(epsilon) <= 0) {
                return new Solution(iteration, solution, difference);
            }
        } while (iteration < iterations);

        throw new SolvingException();
    }
}
