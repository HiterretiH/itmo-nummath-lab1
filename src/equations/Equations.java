package equations;

import exceptions.InvalidEquationException;

import java.math.BigDecimal;
import java.util.Arrays;

public class Equations {
    private final BigDecimal[][] a;
    private final BigDecimal[] b;

    public Equations(BigDecimal[][] matrix) throws InvalidEquationException {
        if (matrix.length == 0 || !validateMatrix(matrix)) {
            throw new InvalidEquationException();
        }

        int size = matrix.length;
        a = new BigDecimal[size][size];
        b = new BigDecimal[size];

        for (int i = 0; i < size; i++) {
            a[i] = Arrays.copyOf(matrix[i], size);
            b[i] = matrix[i][size];
        }
    }

    private boolean validateMatrix(BigDecimal[][] matrix) {
        for (BigDecimal[] eq : matrix) {
            if (eq.length != matrix.length + 1) {
                return false;
            }
        }

        return true;
    }

    public void print() {
        int size = a.length;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(a[i][j].toString().replace("E", "*10^") + " ");
            }
            System.out.println(b[i].toString().replace("E", "*10^"));
        }
    }

    public BigDecimal[][] getA() {
        return a;
    }

    public BigDecimal[] getB() {
        return b;
    }
}
