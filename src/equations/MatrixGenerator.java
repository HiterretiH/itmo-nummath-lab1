package equations;

import java.math.BigDecimal;

public class MatrixGenerator {
    public static BigDecimal[][] generate(int size, double min, double max) {
        BigDecimal[][] matrix = new BigDecimal[size][size + 1];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size + 1; j++) {
                matrix[i][j] = new BigDecimal(Math.round((max - min) * Math.random() + min));
            }
        }

        for (int i = 0; i < size; i++) {
            BigDecimal sum = new BigDecimal(0);
            int maxIndex = size - i - 1;

            for (int j = 0; j < size; j++) {
                if (j != maxIndex) {
                    sum = sum.add(matrix[i][j].abs());
                }
            }

            matrix[i][maxIndex] = sum.add(BigDecimal.ONE);
        }

        return matrix;
    }
}
