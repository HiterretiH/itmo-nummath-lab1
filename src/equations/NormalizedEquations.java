package equations;

import exceptions.InvalidEquationException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

public class NormalizedEquations {
    private final BigDecimal[][] c;
    private final BigDecimal[] d;

    public NormalizedEquations(Equations equations) throws InvalidEquationException {
        BigDecimal[][] a = Arrays.copyOf(equations.getA(), equations.getA().length);
        BigDecimal[] b = Arrays.copyOf(equations.getB(), equations.getB().length);

        int size = a.length;

        c = new BigDecimal[size][size];
        d = new BigDecimal[size];

        for (int i = 0; i < size; i++) {
            if (a[i][i].compareTo(BigDecimal.ZERO) == 0) {
                throw new InvalidEquationException();
            }

            for (int j = 0; j < size; j++) {
                if (i == j) {
                    c[i][j] = BigDecimal.ZERO;
                }
                else {
                    c[i][j] = a[i][j].divide(a[i][i], a[i][j].toString().length() + a[i][i].toString().length(), RoundingMode.HALF_DOWN).negate().stripTrailingZeros();
                }
            }
            d[i] = b[i].divide(a[i][i], a[i][i].toString().length() + b[i].toString().length(), RoundingMode.HALF_DOWN).stripTrailingZeros();
        }
    }

    public BigDecimal[][] getC() {
        return c;
    }

    public BigDecimal[] getD() {
        return d;
    }
}
