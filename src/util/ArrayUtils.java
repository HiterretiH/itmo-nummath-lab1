package util;

import java.math.BigDecimal;

public class ArrayUtils {
    public static void swapArrays(BigDecimal[] a, BigDecimal[] b) {
        int size = Math.min(a.length, b.length);
        BigDecimal temp;
        for (int i = 0; i < size; i++) {
            temp = a[i];
            a[i] = b[i];
            b[i] = temp;
        }
    }

    public static void swapElements(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static void swapElements(BigDecimal[] a, int i, int j) {
        BigDecimal temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static String toString(BigDecimal[] a) {
        StringBuilder result = new StringBuilder();
        for (BigDecimal el : a) {
            result.append(el.stripTrailingZeros().toString().replace("E", "*10^"));
            result.append('\n');
        }

        return result.toString();
    }
}
